/** <module> Abduction meta-interpreter

	This module implements a basic abduction engine to find
	explainations from observed facts.
*/
:-module(abduction, [abduce/2]).

%! abduce(+Observation:Goal, -Explaination:list) is nondet.
%
% Given a Theory and an Observation, find an Explaination
% such that:
%
%	Theory U Explaination :- Observation
%
% The Observation follows logically from the Theory extended
% with the Explaination.
%
% @arg Observation is the starting Goal to prove.
%
% @arg Explaination is the list of abducible literals.
%

abduce(Observation,Explaination):-
	abduce(Observation,[],Explaination).
	
%! abduce(A, E, E) is nondet.
%
% Extends abduce/2 with accumulator for explainations.
%

abduce(true,E,E):- !.

abduce(not(A),E0,E):-
	!,
	not(member(A,E0)),
	abduce_not(A,E0,E).
	
abduce((A,B),E0,E):-
	!,
	abduce(A,E0,E1),
	abduce(B,E1,E).
	
abduce(A,E0,E):-
	clause(A,B),
	abduce(B,E0,E).
	
abduce(A,E,E):- member(A,E).

abduce(A,E,[A|E]):-
	not(member(A,E)),
	abducible(A),
	not(abduce_not(A,E,E)).

%! abduce_not(A, E, E) is nondet.
%
% Closely mirrors the clauses for abduce/3 by explicitly
% constructing an explaination for negated clause.
%
	
abduce_not(not(A),E0,E):-
	not(member(not(A),E0)),
	abduce(A,E0,E).
	
abduce_not((A,B),E0,E):-
	!,
	abduce_not(A,E0,E); abduce_not(B,E0,E).
	
abduce_not(A,E0,E):-
	setof(B,clause(A,B),L),
	abduce_not_lst(L,E0,E).
	
abduce_not(A,E,E):- member(not(A),E).

abduce_not(A,E,[not(A)|E]):-
	not(member(not(A),E)),
	abducible(A),
	not(abduce(A,E,E)).

abduce_not_lst([],E,E).

abduce_not_lst([H|T],E0,E):-
	abduce_not(H,E0,E1),
	abduce_not_lst(T,E1,E).

abducible(A):- A \= not(B), not(clause(A,B)).

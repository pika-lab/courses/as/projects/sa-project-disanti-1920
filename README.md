# Abduction

Abduction extends default reasoning by not only making assumptions about what is false, but also about what is true. The general problem of abduction can be stated as follows: given a _Theory_ and an _Observation_, find an _Explaination_ such that 

<center> Theory U Explaination :- Observation </center>  

## Abductive meta-interpreter

The meta-interpreter given in this repo try to construct an abductive explaination by trying to prove the _Observation_ from the initial _Theory_ alone: whenever a literal for which there is no clause to resolve with is encountered, this will be added to the _Explaination_.

## How to use

Start [SWIProlog](https://www.swi-prolog.org/) and load the `abduction.pl` file, after this the meta-interpreter will be ready to abduce the needed explaination by simply calling the `abduce/2` predicate.

### Example

As an example abduction can be used for formulating hypotheses about faulty components in a malfunctioning system.

+ Start SWIProlog inside this folder
    - `swipl`
+ Load the meta-interpreter
    - `[abduction].`
+ Load the example
    - `[circuit_faults].`
+ Diagnose the circuit
    - `abduce(adder(a,0,0,1,0,1), Diagnosis).`
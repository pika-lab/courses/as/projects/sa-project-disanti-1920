:- begin_tests(abduction).
:-use_module(abduction).

test(abduce, [nondet]) :-
    abduce(true, _).
test(abduce, [true(A==[not(a)]), nondet]) :-
    abduce(not(a), A).
test(abduce, [true(A==[b, a]), nondet]) :-
    abduce((a, b), A).
test(abduce, [true(A==[a]), nondet]) :-
    abduce(a, A).
test(abduce, [true(A==[b, a]), nondet]) :-
    abduce((a, b), A).
test(abduce, [true(A==[b, not(a)]), nondet]) :-
    abduce((not(a), b), A).
test(abduce, [true(A==[not(b), a]), nondet]) :-
    abduce((a, not(b)), A).
test(abduce, [true(A==[a]), nondet]) :-
    abduce(not(not(a)), A).
test(abduce, [true(A==[not(b), not(a)]), nondet]) :-
    abduce((not(a), not(b)), A).
test(abduce, [true(A==[not(not(a), not(b))]), nondet]) :-
    abduce(not(not(a), not(b)), A).
test(abduce, [true(A==[not(a, b)]), nondet]) :-
    abduce(not(a, b), A).
test(abduce, [true(A==[a]), nondet]) :-
    abduce(not(not(a)), A).
test(abducible, [nondet]) :-
    abduction:abducible(a).
test(abduce_not, [true(A==[not(a)]), nondet]) :-
    abduction:abduce_not(a, [], A).
test(abduce_not, [true(A==[a]), nondet]) :-
    abduction:abduce_not(not(a), [], A).

:- end_tests(abduction).
